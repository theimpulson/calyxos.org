---
title: September 2024 Security update, take two
date: 2024-09-08
---

* CalyxOS 5.11.1 - September 2024 Security update for Pixel 3 - 5, FP4, FP5 and Motos
* Android 15 work in progress

## Rollout

### Pixel 5 series, FP4, FP5 and Motos

| Release channel  | Date   | Notes |
| ---------------- | ------ | ------ |
| Security express | 8 September, Sunday |  |
| Beta | 9 September, Monday |  |
| Stable | 10 September, Tuesday | |

### Pixel 3, 3a, 4, 4a

| Release channel  | Date   | Notes |
| ---------------- | ------ | ------ |
| Security express | 9 September, Monday |  |
| Beta | 9 September, Monday |  |
| Stable | 10 September, Tuesday | |

## Changelog
* CalyxOS 5.11.1
* September 2024 Security update
* Chromium 128.0.6613.127

### FP4
* Fix MPEG media playback hangs

### FP5
* Update to FP5.UT27.B.059.20240809

## Note

{% include install/security_notes.html %}

## Matrix channel

* If you're having trouble trying to post messages to the [#CalyxOS](https://matrix.to/#/#CalyxOS:matrix.org) matrix channel, try to leave the channel if possible, and then try writing `/join #CalyxOS:matrix.org` in any chat in Matrix. That should re-connect you to the channel.
